CURDIR := $(shell pwd)
BASEDIR := $(abspath $(CURDIR))
PUBLICDIR := $(BASEDIR)/public
SOURCEDIR := $(BASEDIR)/source

PROJECT := otp-crash-course-2021

.PHONY: ci-build ci-load ci-save docker-build docker-publish docker-shell

ci-build::
	if [ -f "$(BASEDIR)/ci/image.tar.gz" ]; then \
		$(MAKE) ci-load; \
	else \
		$(MAKE) docker-build; \
		$(MAKE) ci-save; \
	fi

ci-load::
	docker load -i "$(BASEDIR)/ci/image.tar.gz"

ci-save::
	mkdir -p "$(BASEDIR)/ci" && \
	docker save $(PROJECT):latest | gzip > "$(BASEDIR)/ci/image.tar.gz"

docker-build::
	docker build \
		-t $(PROJECT):latest \
		-f $(SOURCEDIR)/Dockerfile \
		$(SOURCEDIR)

docker-publish::
	rm -rf $(PUBLICDIR) && \
	mkdir -p $(PUBLICDIR) && \
	docker run \
		-v "$(PUBLICDIR)":"/build/public" \
		-v "$(SOURCEDIR)":"/build/source" \
		--rm \
		$(PROJECT):latest \
		/bin/bash -l -c 'make all && rsync -a _book/ ../public/'

docker-shell::
	docker run \
		-v "$(SOURCEDIR)":"/build/source" \
		--rm \
		-it $(PROJECT):latest \
		/bin/bash -l

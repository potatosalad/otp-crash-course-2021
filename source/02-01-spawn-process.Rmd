## `spawn` process

_TL;DR:_ A pre-emptive concurrency primitive.  Use sparingly.

- **Use-Cases**
    - _Opinion:_ You should use this sparingly.  It's pretty rare when you have a true fire-and-forget use-case.
    - Fire-and-forget tasks
    - Garbage collection optimized control (on exit, garbage collect the process)
- **Summary**
    - **MFA**
        - [`erlang:spawn/1`](https://erlang.org/doc/man/erlang.html#spawn-1) - spawns a new process
        - [`erlang:exit/1`](https://erlang.org/doc/man/erlang.html#exit-1) - exit current process
        - [`erlang:exit/2`](https://erlang.org/doc/man/erlang.html#exit-2) - force another process to exit
	- **Start Type:** ASYNC
    - **Monitored:** NO
    - **Linked:** NO
    - **Trap Exit:** NO
    - **Normal Exit**
        - ⚠️ Does NOT notify other process
        - ✅	 Does NOT crash other process
    - **Abnormal Exit**
        - ❌ Does NOT notify other process
        - ⚠️ Does NOT crash other process
    - **Uncaught Error**
        - ❌ Does NOT notify other process
        - ⚠️ Does NOT crash other process
        - ✅	[May log error report](https://erlang.org/doc/system_principles/error_logging.html)

### Minimal Example

```erl
1> % Is the parent process alive?
1> ParentPid = erlang:self(),
1> erlang:is_process_alive(ParentPid).
true

2> % What happens when a child process exits normally?
2> NormalPid = erlang:spawn(fun() -> erlang:exit(normal) end),
2> erlang:is_process_alive(NormalPid).
false

3> % What happens when a child process exits abnormally?
3> AbnormalPid = erlang:spawn(fun() -> erlang:exit(abnormal) end),
3> erlang:is_process_alive(AbnormalPid).
false

4> % What happens when a child process errors?
4> CrashPid = erlang:spawn(fun() -> erlang:error(crash) end),
4> erlang:is_process_alive(CrashPid).
=ERROR REPORT==== 15-Jun-2021::16:19:35.116686 ===
Error in process <0.169.0> with exit value:
{crash,[{shell,apply_fun,3,[{file,"shell.erl"},{line,907}]}]}

false

5> % Is the original parent process still alive?
5> erlang:is_process_alive(ParentPid).
true

6> % Are there any messages in the mailbox for the parent process?
6> flush().
ok
```

### Server Example

Example usage:

```erl
1> Pid = ex1:spawn(),
1> Pid ! foo,
1> timer:sleep(3000),
1> Pid ! stop.
<0.154.0> Start
<0.154.0> State = 0
<0.154.0> Msg = foo
<0.154.0> State = 0
<0.154.0> State = 1
<0.154.0> State = 2
<0.154.0> Stop
stop
```

Example module:

```erlang
-module(ex1).
%% BEAM process
%%
%% - Start with `Pid = ex1:spawn()`
%% - Stop with `Pid ! stop`
%% - Increments the `State + 1` every second no message is received
%% - Prints `Msg = ~p` to the screen for every other `Pid ! term()` received
%%

-export([spawn/0]).

-spec spawn() -> pid().
spawn() ->
    erlang:spawn(fun init/0).

%% @private
init() ->
    io:format("~p Start~n", [erlang:self()]),
    State = 0,
    loop(State).

%% @private
loop(State) ->
    io:format("~p State = ~p~n", [erlang:self(), State]),
    receive
        stop ->
            io:format("~p Stop~n", [erlang:self()]),
            erlang:exit(normal);
        Msg ->
            io:format("~p Msg = ~p~n", [erlang:self(), Msg]),
            loop(State)
    after
        1000 ->
            loop(State + 1)
    end.
```

### Diagram

```{r 02-01-spawn-process-diag1, echo=FALSE, fig.cap='Diagram for `spawn` process exit states.', out.width='75%'}
knitr::include_graphics('diagrams/02-01-spawn-process-diag1.svg', dpi = NA)
```
